Actualizar los Paquetes del Sistema
Este comando actualiza la lista de paquetes del sistema operativo. Se recomienda ejecutarlo antes de instalar cualquier software nuevo para asegurarse de que se descarguen las versiones más recientes de los paquetes y dependencias necesarios.
sudo apt-get update
Instalar PostgreSQL y los Componentes Adicionales
Este comando instala PostgreSQL y el paquete postgresql-contrib, que incluye extensiones y herramientas adicionales útiles para gestionar bases de datos en PostgreSQL.

sudo apt install postgresql postgresql-contrib
Cambiar al Usuario 'postgres'
PostgreSQL crea un usuario del sistema llamado postgres por defecto. Este comando cambia el usuario actual al usuario postgres, permitiéndote interactuar directamente con el servidor PostgreSQL desde la línea de comandos.

sudo su - postgres
Abrir la Consola de PostgreSQL
Este comando abre la consola interactiva de PostgreSQL desde la cual puedes ejecutar comandos SQL directamente en tu base de datos.

psql
Crear un Usuario en PostgreSQL
Este comando SQL crea un nuevo usuario en PostgreSQL llamado prueba con la contraseña Prueba2022. Los usuarios son necesarios para gestionar y acceder a las bases de datos en PostgreSQL.

CREATE USER prueba WITH PASSWORD 'Prueba2022';
Crear una Base de Datos y Asignar un Propietario
Este comando crea una base de datos llamada db_prueba y asigna como propietario al usuario prueba. El propietario de la base de datos tiene privilegios especiales sobre esa base de datos.

CREATE DATABASE db_prueba OWNER prueba;
Otorgar Privilegios de Superusuario a un Usuario
Este comando otorga privilegios de superusuario al usuario prueba, permitiéndole realizar operaciones avanzadas y administrativas en la base de datos.

ALTER USER prueba WITH SUPERUSER;
Instalación de PgAdmin
Añadir la Llave del Repositorio de PgAdmin
Este comando descarga y añade la llave GPG del repositorio de PgAdmin, asegurando que los paquetes instalados desde el repositorio son seguros y confiables.

sudo curl https://www.pgadmin.org/static/packages_pgadmin_org.pub | sudo apt-key add -
Agregar el Repositorio de PgAdmin
Este comando agrega el repositorio oficial de PgAdmin a tu lista de fuentes de software. El repositorio contiene los paquetes necesarios para instalar PgAdmin.

sudo sh -c '. /etc/upstream-release/lsb-release && echo "deb https://ftp.postgresql.org/pub/pgadmin/pgadmin4/apt/$DISTRIB_CODENAME pgadmin4 main" > /etc/apt/sources.list.d/pgadmin4.list && apt update'
Instalar PgAdmin
Finalmente, este comando instala PgAdmin en tu sistema, permitiéndote gestionar bases de datos PostgreSQL desde una interfaz gráfica fácil de usar.

sudo apt install pgadmin4

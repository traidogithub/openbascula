package fer.openBascula.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class MainWindowV2015 extends JFrame {

	// Componentes GUI
	private JTextField txtCabina, txtRemolque, txtPesoEntrada, txtPesoSalida, txtNeto, txtProducto, txtCliente;
	private JButton btnProductos, btnCliente, btnNuevo, btnTransito;
	private JLabel lblCabina, lblRemolque, lblPesoEntrada, lblPesoSalida, lblNeto, lblProducto, lblCliente;
	// private JDatePicker dtFechaEntrada; // Necesitarás usar una biblioteca para
	// esto (p. ej., JDatePicker)

	public MainWindowV2015() {
		setTitle("APP NAME v1.0");
		setSize(1000, 650);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLocationRelativeTo(null);

		initComponents();
		layoutComponents();
		initEvents();
	}

	private void initComponents() {
		// Inicializar componentes
		lblCabina = new JLabel("CABINA");
		lblRemolque = new JLabel("REMOLQUE");
		lblPesoEntrada = new JLabel("Kg. ENTRADA");
		lblPesoSalida = new JLabel("Kg. SALIDA");
		lblNeto = new JLabel("NETO");
		lblProducto = new JLabel("PRODUCTO");
		lblCliente = new JLabel("CLIENTE");

		txtCabina = new JTextField(20);
		txtRemolque = new JTextField(20);
		txtPesoEntrada = new JTextField(20);
		txtPesoSalida = new JTextField(20);
		txtNeto = new JTextField(20);
		txtProducto = new JTextField(20);
		txtCliente = new JTextField(20);

		btnProductos = new JButton("Buscar Producto");
		btnCliente = new JButton("Buscar Cliente");
		btnNuevo = new JButton("Nuevo");
		btnTransito = new JButton("En Transito");

		// Puedes añadir más componentes según sea necesario
	}

	private void layoutComponents() {
		// Organizar los componentes en la ventana
		JPanel panel = new JPanel(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.insets = new Insets(5, 5, 5, 5);

		// Añadir componentes al panel
		gbc.gridx = 0;
		gbc.gridy = 0;
		panel.add(lblCabina, gbc);

		gbc.gridx = 1;
		gbc.gridy = 0;
		panel.add(txtCabina, gbc);

		gbc.gridx = 0;
		gbc.gridy = 1;
		panel.add(lblRemolque, gbc);

		gbc.gridx = 1;
		gbc.gridy = 1;
		panel.add(txtRemolque, gbc);

		gbc.gridx = 0;
		gbc.gridy = 2;
		panel.add(lblPesoEntrada, gbc);

		gbc.gridx = 1;
		gbc.gridy = 2;
		panel.add(txtPesoEntrada, gbc);

		gbc.gridx = 0;
		gbc.gridy = 3;
		panel.add(lblPesoSalida, gbc);

		gbc.gridx = 1;
		gbc.gridy = 3;
		panel.add(txtPesoSalida, gbc);

		gbc.gridx = 0;
		gbc.gridy = 4;
		panel.add(lblNeto, gbc);

		gbc.gridx = 1;
		gbc.gridy = 4;
		panel.add(txtNeto, gbc);

		gbc.gridx = 0;
		gbc.gridy = 5;
		panel.add(lblProducto, gbc);

		gbc.gridx = 1;
		gbc.gridy = 5;
		panel.add(txtProducto, gbc);

		gbc.gridx = 0;
		gbc.gridy = 6;
		panel.add(lblCliente, gbc);

		gbc.gridx = 1;
		gbc.gridy = 6;
		panel.add(txtCliente, gbc);

		gbc.gridx = 0;
		gbc.gridy = 7;
		panel.add(btnProductos, gbc);

		gbc.gridx = 1;
		gbc.gridy = 7;
		panel.add(btnCliente, gbc);

		gbc.gridx = 0;
		gbc.gridy = 8;
		panel.add(btnNuevo, gbc);

		gbc.gridx = 1;
		gbc.gridy = 8;
		panel.add(btnTransito, gbc);

		add(panel);
	}

	private void initEvents() {
		// Asignar eventos
		btnNuevo.addActionListener(e -> onNuevoClicked());
		btnProductos.addActionListener(e -> onProductosClicked());
		btnCliente.addActionListener(e -> onClienteClicked());
	}

	private void onNuevoClicked() {
		JOptionPane.showMessageDialog(this, "Nuevo clicado!");
	}

	private void onProductosClicked() {
		JOptionPane.showMessageDialog(this, "Buscar Producto clicado!");
	}

	private void onClienteClicked() {
		JOptionPane.showMessageDialog(this, "Buscar Cliente clicado!");
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> {
			new MainWindowV2015().setVisible(true);
		});
	}

}

package fer.openBascula.gui;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.UIManager;

import fer.openBascula.interfaces.IConstant;
import fer.openBascula.interfaces.IDesktopWindow;

public class VPrincipal implements IDesktopWindow {

	private JTextField txtCabina, txtRemolque, txtPesoEntrada, txtPesoSalida, txtNeto, txtProducto, txtCliente;
	private JButton btnProductos, btnCliente, btnNuevo, btnTransito;
	private JLabel lblCabina, lblRemolque, lblPesoEntrada, lblPesoSalida, lblNeto, lblProducto, lblCliente;
	private JFrame frame;
	private IConstant constantes;

	public VPrincipal() {
		constantes = new fer.openBascula.structures.constasts.V2025();
	}

	@Override
	public void showWindow() {
		UIManager.LookAndFeelInfo[] looks = UIManager.getInstalledLookAndFeels();
		/*
		 * for (UIManager.LookAndFeelInfo look : looks) {
		 * System.out.println(look.getClassName()); }
		 */
		try {
			UIManager.setLookAndFeel(constantes.LOOK_AND_FEEL());
		} catch (Exception ignored) {
		}

		frame.setVisible(true);
	}

	@Override
	public void initComponents() {

		frame = new JFrame(constantes.APP_NAME() + " " + constantes.APP_VERSION());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(constantes.WIDTH(), constantes.HEIGHT());

		// Inicializar componentes
		lblCabina = new JLabel("CABINA");
		lblRemolque = new JLabel("REMOLQUE");
		lblPesoEntrada = new JLabel("Kg. ENTRADA");
		lblPesoSalida = new JLabel("Kg. SALIDA");
		lblNeto = new JLabel("NETO");
		lblProducto = new JLabel("PRODUCTO");
		lblCliente = new JLabel("CLIENTE");

		txtCabina = new JTextField(20);
		txtRemolque = new JTextField(20);
		txtPesoEntrada = new JTextField(20);
		txtPesoSalida = new JTextField(20);
		txtNeto = new JTextField(20);
		txtProducto = new JTextField(20);
		txtCliente = new JTextField(20);

		btnProductos = new JButton("Buscar Producto");
		btnCliente = new JButton("Buscar Cliente");
		btnNuevo = new JButton("Nuevo");
		btnTransito = new JButton("En Transito");

	}

	@Override
	public void initDesign() {
		// TODO Auto-generated method stub

	}
}

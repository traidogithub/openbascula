package fer.openBascula.gui;

import java.util.Arrays;

import fer.openBascula.interfaces.IDesktopWindow;

public class init {

	public static void main(String[] args) {
		IDesktopWindow app = null;

		System.out.println("Argumentos pasados por linea de comandos:");
		for (var argumento : args) {
			System.out.println("\t" + argumento);
		}

		if (args.length ==0 || Arrays.asList(args).contains("--v2025")) {
			System.out.println("Version 2025");
			app = new VPrincipal();
			app.initComponents();
			app.initDesign();
		} else {
			System.out.println("Version no reconocida");
		}

		if (app != null) {
			app.showWindow();
		}
	}

}

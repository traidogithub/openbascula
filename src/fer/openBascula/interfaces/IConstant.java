package fer.openBascula.interfaces;

public interface IConstant {
	String APP_NAME();

	String APP_VERSION();

	String LOOK_AND_FEEL();

	int WIDTH();

	int HEIGHT();

}

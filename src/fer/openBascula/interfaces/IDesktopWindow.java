package fer.openBascula.interfaces;

public interface IDesktopWindow {
	/**
	 * Show the window
	 */
	void showWindow();

	/**
	 * Inicialize visual components
	 */
	void initComponents();

	/**
	 * Inicialize design layout
	 */
	void initDesign();
}

/**
 * 
 */
package fer.openBascula.pruebas;

import javax.swing.JFrame;
import javax.swing.UIManager;

import fer.openBascula.interfaces.IConstant;
import fer.openBascula.structures.constasts.V2025;

import java.util.Scanner;

/**
 * 
 */
public class init {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		boolean salir = false;

		do {
			System.out.print("\r\nInicio de las pruebas" +
					"\r\n\t1.- Nueva ventana" +
					"\r\n\t2.- JDBC postgresSQL" +
					"\r\n\t--------------------------" +
					"\r\n\ts.- Salir");
			System.out.print("\r\nInserte opcion>");
			var opcion = input.nextLine();


			switch (opcion) {
				case "1":
					newWindow();
					break;
				case "2":
					probarJDBC_postgreSQL();
					break;
				case "s":
					salir = true;
					break;
				default:
					System.out.println("opcion no reconocida");
			}
		}while (salir == false);

		System.out.println("Fin");
	}

	private static void probarJDBC_postgreSQL() {
		var p = new pruebaJDBC();
		p.probarLibreria();
		p.connectDatabase();
	}

	private static void newWindow() {
		IConstant v2025 = new V2025();
		JFrame frame = new JFrame(v2025.APP_NAME());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(300, 300);
		UIManager.LookAndFeelInfo[] looks = UIManager.getInstalledLookAndFeels();
		for (UIManager.LookAndFeelInfo look : looks) {
			System.out.println(look.getClassName());
		}
		try {
			UIManager.setLookAndFeel(v2025.LOOK_AND_FEEL());
		} catch (Exception ignored) {
		}

		frame.setVisible(true);
	}

}

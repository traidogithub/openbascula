package fer.openBascula.pruebas;
import java.sql.*;

public class pruebaJDBC {
    public final String DB_NAME = "openbasculadb";
    public final String USER = "fer";
    public final String PASS = "toor";

    /**
     * Prueba para ver si se carga la libreria
     */
    public void probarLibreria(){
        try {
            Class.forName("org.postgresql.Driver");
            System.out.println("Exito");
        } catch (ClassNotFoundException ex) {
            System.out.println("Error al registrar el driver de PostgreSQL: " + ex);
        }
    }

    /**
     * Prueba de conexion
     */
    public void connectDatabase() {
        try {
            // We register the PostgreSQL driver
            // Registramos el driver de PostgresSQL
            try {
                Class.forName("org.postgresql.Driver");
            } catch (ClassNotFoundException ex) {
                System.out.println("Error al registrar el driver de PostgreSQL: " + ex);
            }
            Connection connection = null;
            // Database connect
            // Conectamos con la base de datos
            connection = DriverManager.getConnection(
                    "jdbc:postgresql://localhost:5432/" + DB_NAME,
                    USER, PASS);

            boolean valid = connection.isValid(50000);
            System.out.println(valid ? "TEST OK" : "TEST FAIL");
        } catch (java.sql.SQLException sqle) {
            System.out.println("Error: " + sqle);
        }
    }
}

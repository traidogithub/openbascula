package fer.openBascula.structures.constasts;

import fer.openBascula.interfaces.IConstant;

public class V2025 implements IConstant {
	@Override
	public String APP_NAME() {
		return "Open Bascula";
	}

	@Override
	public String APP_VERSION() {
		return "25.02";
	}

	@Override
	public String LOOK_AND_FEEL() {
		return "javax.swing.plaf.nimbus.NimbusLookAndFeel";
	}

	@Override
	public int WIDTH() {
		return 1000;
	}

	@Override
	public int HEIGHT() {
		return 650;
	}
}
